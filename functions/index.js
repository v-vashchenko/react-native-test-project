const functions = require('firebase-functions');
const config = require('./config.json');
const axios = require('axios');

exports.categoriesList = functions.https.onRequest(async (req, res) => {
  const { apiKey } = config;
  const queryString = `https://api.nytimes.com/svc/books/v3/lists/names.json?api-key=${apiKey}`;
  try {
    const data = await axios({
      method: "GET",
      url: queryString
    });
    const { results } = data.data;
    return res.status(200).json({ data: results });
  } catch (err) {
    console.log('@@@err', err);
    return res.status(500).json({ 'error': err });
  }
});

exports.booksList = functions.https.onCall(async (input, context) => {
  const { apiKey } = config;
  const { listName = '', offset = 0 } = input;

  if (!listName) {
    throw new functions.https.HttpsError('invalid-argument', 'Specify list name!');
  }

  let queryString = `https://api.nytimes.com/svc/books/v3/lists/current/`;
  queryString += `${listName}.json`;
  if (offset) {
    queryString += `?offset=${offset}`;
  }
  queryString += `?api-key=${apiKey}`;

  console.log('@@@queryString', queryString);

  try {

    const { data } = await axios({
      method: "GET",
      url: queryString
    });

    console.log('@@@data', data);

    return data;

  } catch (err) {
    console.log('@@@err', err);
    throw new functions.https.HttpsError('internal', `Error:${err}`);
  }
});
