import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  content: { flex: 1, paddingTop: 10, paddingLeft: 5, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  avatar: { width: 100, height: 100, borderRadius: 50 },
  avatarContainer: {
  }
});
