import { connect } from 'react-redux';

import * as booksActions from '../../store/books/books.actions';

const mapStateToProps = ({ books }) => ({
  categoriesList: books.categoriesList,
  loadingData: books.loadingData,
});

const mapDispatchToProps = {
  ...booksActions,
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
