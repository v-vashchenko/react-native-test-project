import React, { Component } from 'react'
import { NavigationHeader } from '../Common/NavigationHeader';
import moment from 'moment';
import propTypes from 'prop-types';

import { Image, TouchableOpacity } from 'react-native'
import { Container, Text, Icon, Form, Item, Input, DatePicker, Content, View, Card, CardItem, Body } from 'native-base';

import connect from './connect';

import Spinner from '../Common/Spinner/Spinner'
import styles from './styles';

import ButtonsBar from '../Common/ConfirmButtonsBar'

import { leftTrim } from '../utils/index';


class CategoriesList extends Component {

  async componentDidMount() {
    console.log('@@@here');
    if (!this.props.categoriesList.length) {
      this.props.changeBooksLodaingDataStatus();
      await this.props.getCategories();
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <NavigationHeader
          title='Categories'
          navigation={navigation}
          showBackButton
        />
      ),
    };
  }

  onCardPress = async (listName) => {
    this.props.changeBooksLodaingDataStatus()
    await this.props.getBooks(listName);
    this.props.navigation.navigate('BooksList')
  }

  render() {

    if (this.props.loadingData) {
      return <Spinner />
    }

    const { categoriesList = [] } = this.props;

    return (
      <Container>
        <Content>
          {categoriesList.map((el, index) => {
            const { display_name, newest_published_date, list_name_encoded } = el;
            return (
              <Card
                key={list_name_encoded + index}
              >
                <CardItem
                  button
                  onPress={() => this.onCardPress(list_name_encoded)}
                >
                  <Body>
                    <Text style={styles.title}
                    >
                      {display_name}
                    </Text>
                    <Text style={styles.subtitle}
                    >
                      upd.{newest_published_date}
                    </Text>
                  </Body>
                </CardItem>
              </Card>
            )
          })}
        </Content>
      </Container>
    )
  }
}

CategoriesList.propTypes = {
  categoriesList: propTypes.arrayOf(propTypes.shape())
};

CategoriesList.defaultProps = {
  categoriesList: []
};

export default connect(CategoriesList);