import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { flex: 1, padding: 5, paddingTop: 10, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'flex-start' },
  row: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#000',
    borderBottomWidth: 0.5,
    borderRightWidth: 0,
    maxHeight: 10
  },
  cell: {
    flex: 1,
    borderRightWidth: 1,
    width: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: { fontSize: 6 },
  email: {
    minWidth: 50,
    alignItems: 'flex-start',
    paddingLeft: 5
  },
  emailText: {
    textAlign: 'left'
  },
  controls: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10
  },
  button: {
    backgroundColor: '#F48024',
    marginLeft: 10,
  }
});
