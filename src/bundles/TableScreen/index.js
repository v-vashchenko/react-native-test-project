import React, { Component } from 'react';
import propTypes from 'prop-types';
import faker from 'faker';
import { View, Button, Icon, Text } from 'native-base';

import connect from './connect';
import { NavigationHeader } from '../Common/NavigationHeader';
import styles from './styles';
import Spinner from '../Common/Spinner/Spinner';

class Table extends Component {

  state = {
    data: [],
    page: 1,
    limit: 25
  };

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <NavigationHeader
          title='Fake Table'
          showBackButton
          navigation={navigation}
        />
      ),
    };
  }

  componentWillMount() {
    const { data } = this.state;
    const limit = 100;
    this.props.changeLodaingDataStatusCurrencies();
    for (let i = 0; i < limit; ++i) {
      data.push({
        email: faker.internet.email(),
        url: faker.internet.url(),
        mac: faker.internet.mac(),
        ip: faker.internet.ip(),
      })
    }
    this.props.changeLodaingDataStatusCurrencies();
    this.setState({ data: data });
  };

  getRecords = (page = 1) => {
    const { limit } = this.state;
    let { data } = this.state;
    const start = (page - 1) * limit;
    const end = start + limit;
    data = data.slice(start, end);
    return data;
  }

  onNext = () => {
    let { page } = this.state;
    ++page;
    this.setState({ page })
  }

  onPrevious = () => {
    let { page } = this.state;
    --page;
    this.setState({ page })
  }

  render() {

    const { page } = this.state;

    const isLast = page === this.state.data.length / this.state.limit | 0;

    const data = this.getRecords(page);

    if (this.props.loadingData) {
      return <Spinner />
    }

    return (
      <View style={styles.container}>
        {data.map(el => {
          return (
            <View style={styles.row} key={faker.random.uuid()}>
              <View style={[styles.cell, styles.email]}>
                <Text
                  numberOfLines={1}
                  style={[styles.text, styles.emailText]}
                >
                  {el.email}
                </Text>
              </View>
              <View style={[styles.cell, styles.url]}>
                <Text
                  numberOfLines={1}
                  style={[styles.text, styles.urlText]}
                >
                  {el.url}
                </Text>
              </View>
              <View style={[styles.cell, styles.mac]}>
                <Text
                  numberOfLines={1}
                  style={[styles.text, styles.macText]}
                >
                  {el.mac}
                </Text>
              </View>
              <View style={[styles.cell, styles.ip]}>
                <Text
                  numberOfLines={1}
                  style={[styles.text, styles.ipText]}
                >
                  {el.ip}
                </Text>
              </View>
            </View>
          )
        })}
        <View style={styles.controls}>
          <Button
            style={[styles.button, page === 1 ? { backgroundColor: 'gray' } : null]}
            onPress={page !== 1 ? this.onPrevious : null}
          >
            <Icon
              type='FontAwesome'
              name='step-backward'
            />
          </Button>

          <Button
            style={[styles.button, isLast ? { backgroundColor: 'gray' } : null]}
            onPress={!isLast ? this.onNext : null}
          >
            <Icon
              type='FontAwesome'
              name='step-forward'
            />
          </Button>
        </View>
      </View >
    )
  }
}

Table.propTypes = {
  changeLodaingDataStatusCurrencies: propTypes.func.isRequired,
  loadingData: propTypes.bool.isRequired,
};

export default connect(Table);