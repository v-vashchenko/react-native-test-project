import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  content: { flex: 1, paddingTop: 10, paddingLeft: 5, backgroundColor: '#fff', flexDirection: 'column', justifyContent: 'flex-start', marginBottom: 15, alignItems: 'center' },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },

  preview: {
    width: 50,
    height: 70,
    marginRight: 7
  },
  author: {
    fontWeight: 'bold',
    fontSize: 12,
    color: 'gray'
  },
  rank: {
    fontSize: 14,
    marginBottom: 5
  },
  description: {
    alignSelf: 'stretch',
    borderWidth: 1,
    borderRadius: 25,
    borderColor: 'gray',
    padding: 10
  },
  descriptionText: {
    fontSize: 18,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: "flex-end",
    alignSelf: 'stretch',
    alignItems: 'flex-end',
    marginBottom: 10,
    paddingRight: 10
  },
  iconContainer: {
    marginLeft: 5,
    width: 35,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 5
  },
  url: {
    color: '#0645AD',
    textDecorationLine: 'underline'
  }
});
