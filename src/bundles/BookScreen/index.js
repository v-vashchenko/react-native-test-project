import React, { Component } from 'react'
import { NavigationHeader } from '../Common/NavigationHeader';
import moment from 'moment';
import propTypes from 'prop-types';

import { Image, TouchableOpacity, Linking } from 'react-native'
import { Container, Text, Icon, Form, Item, Input, DatePicker, Content, View, Card, CardItem, Body } from 'native-base';

import connect from './connect';

import Spinner from '../Common/Spinner/Spinner'
import styles from './styles';

import { leftTrim } from '../utils/index';
import { ScrollView } from 'react-native-gesture-handler';


class BookScreen extends Component {

  componentWillMount() {
    this.props.navigation.setParams({ title: this.props.bookData ? this.props.bookData.title : 'Books' });
  }

  async componentDidMount() {
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      header: (
        <NavigationHeader
          {...params}
          navigation={navigation}
          showBackButton
        />
      ),
    };
  }

  goToUrl = (url) => {
    if (!url) return
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log('Don\'t know how to open URI: ' + url);
      }
    })
  };

  onStar = async (bookData) => {
    const { globalUser } = this.props
    console.log('@@@globalUser', globalUser);
    console.log('@@@bookData', bookData);
    await this.props.starBook(globalUser, bookData);
    await this.props.uptateGlobalUser();
  }

  render() {

    const { bookData: {
      book_image_width,
      book_image_height,
      book_image,
      author,
      rank,
      rank_last_week,
      description,
      amazon_product_url,
      sunday_review_link,
      primary_isbn10
    } } = this.props;

    console.log('@@@this.props.globalUser', this.props.globalUser);

    console.log('@@@this.props.bookData', this.props.bookData);

    const imageRatio = book_image_height / book_image_width;

    const icon = rank > rank_last_week ? { name: 'caret-up', color: 'green' } : { name: 'caret-down', color: 'red' };

    const height = 250;
    const width = height / imageRatio;

    let starredBooks = [];

    if (this.props.globalUser) {
      starredBooks = this.props.globalUser.starredBooks;
    }

    const starName = starredBooks.find(el => el.primary_isbn10 === primary_isbn10) ? 'star' : 'star-outline';

    if (this.props.loadingData) {
      return <Spinner />
    }

    return (
      <Container style={styles.content}>
        <ScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }}>
          <View
            style={styles.buttons}
          >
            <TouchableOpacity
              style={styles.iconContainer}
              onPress={() => this.goToUrl(amazon_product_url)}
            >
              <Icon
                type='FontAwesome'
                name='amazon'
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.iconContainer}
              onPress={() => this.onStar(this.props.bookData)}
            >
              <Icon
                name={starName}
              />
            </TouchableOpacity>


          </View>
          <Image
            style={[{ width, height }]}
            source={{ uri: book_image }}
          />

          <Text style={styles.author}>
            by: {author}
          </Text>

          <Text style={styles.rank}>Rank: {' '}
            <Icon
              type='FontAwesome'
              name={icon.name}
              style={{ color: icon.color }}
            />
            {' ' + rank}
          </Text>
          <TouchableOpacity
            style={styles.description}
            onPress={() => this.goToUrl(sunday_review_link)}
          >
            <Text
              style={styles.descriptionText}
            >
              {description}
              {sunday_review_link
                ? <Text style={styles.url}>More info</Text>
                : null
              }
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </Container >
    )
  }
}

BookScreen.propTypes = {
  booksData: propTypes.shape()
};

BookScreen.defaultProps = {
  booksData: {}
};

export default connect(BookScreen);