import { connect } from 'react-redux';

import * as booksActions from '../../store/books/books.actions';
import * as userActions from '../../store/user/user.actions';

const mapStateToProps = ({ books, user }) => ({
  bookData: books.bookData,
  globalUser: user.globalUser,
});

const mapDispatchToProps = {
  ...booksActions,
  ...userActions,
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
