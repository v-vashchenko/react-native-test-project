import { SCENE_KEYS } from '../constants';

import RateList from '../../RateList';
import ExchangeScreen from '../../ExchangeScreen';
import Login from '../../Auth/Login';
import Signup from '../../Auth/SignUp';
import Restore from '../../Auth/Restore';
import Splash from '../../Splash';
import Profile from '../../Profile';
import CategoriesList from '../../BooksCategories';
import BooksList from '../../BooksList';
import BookScreen from '../../BookScreen';
import TableScreen from '../../TableScreen';

const config = {
  [SCENE_KEYS.Splash]: {
    screen: Splash,
  },
  [SCENE_KEYS.Login]: {
    screen: Login,
  },
  [SCENE_KEYS.Signup]: {
    screen: Signup,
  },
  [SCENE_KEYS.Restore]: {
    screen: Restore,
  },
  [SCENE_KEYS.RateList]: {
    screen: RateList,
  },
  [SCENE_KEYS.ExchangeScreen]: {
    screen: ExchangeScreen,
  },
  [SCENE_KEYS.Profile]: {
    screen: Profile,
  },
  [SCENE_KEYS.CategoriesList]: {
    screen: CategoriesList,
  },
  [SCENE_KEYS.BooksList]: {
    screen: BooksList,
  },
  [SCENE_KEYS.BookScreen]: {
    screen: BookScreen,
  },
  [SCENE_KEYS.TableScreen]: {
    screen: TableScreen,
  },
};

export default config;