export const SCENE_KEYS = {
  Splash: 'Splash',
  Login: 'Login',
  RateList: 'RateList',
  ExchangeScreen: 'ExchangeScreen',
  Signup: 'Signup',
  Restore: 'Restore',
  Profile: 'Profile',
  CategoriesList: 'CategoriesList',
  BooksList: 'BooksList',
  BookScreen: 'BookScreen',
  TableScreen: 'TableScreen',
}

export const emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const CURRENCIES_LIST = [
  {
    "symbol": "$",
    "label": "US Dollar",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "USD",
    'emoji_flag': '🇺🇸',
    "label_plural": "US dollars"
  },
  {
    "symbol": "CA$",
    "label": "Canadian Dollar",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "CAD",
    'emoji_flag': '🇨🇦',
    "label_plural": "Canadian dollars"
  },
  {
    "symbol": "€",
    "label": "Euro",
    "symbol_native": "€",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "EUR",
    'emoji_flag': '🇪🇺',
    "label_plural": "euros"
  },
  {
    "symbol": "AED",
    "label": "Arab Emirates Dirham",
    "symbol_native": "د.إ.‏",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "AED",
    'emoji_flag': '🇦🇪',
    "label_plural": "UAE dirhams"
  },
  {
    "symbol": "AR$",
    "label": "Argentine Peso",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "ARS",
    'emoji_flag': '🇦🇷',
    "label_plural": "Argentine pesos"
  },
  {
    "symbol": "AU$",
    "label": "Australian Dollar",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "AUD",
    'emoji_flag': '🇦🇺',
    "label_plural": "Australian dollars"
  },
  {
    "symbol": "BGN",
    "label": "Bulgarian Lev",
    "symbol_native": "лв.",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "BGN",
    'emoji_flag': '🇧🇬',
    "label_plural": "Bulgarian leva"
  },
  {
    "symbol": "R$",
    "label": "Brazilian Real",
    "symbol_native": "R$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "BRL",
    'emoji_flag': '🇧🇷',
    "label_plural": "Brazilian reals"
  },
  {
    "symbol": "CL$",
    "label": "Chilean Peso",
    "symbol_native": "$",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "CLP",
    'emoji_flag': '🇨🇱',
    "label_plural": "Chilean pesos"
  },
  {
    "symbol": "CN¥",
    "label": "Chinese Yuan",
    "symbol_native": "CN¥",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "CNY",
    'emoji_flag': '🇨🇳',
    "label_plural": "Chinese yuan"
  },
  {
    "symbol": "CO$",
    "label": "Colombian Peso",
    "symbol_native": "$",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "COP",
    'emoji_flag': '🇨🇴',
    "label_plural": "Colombian pesos"
  },
  {
    "symbol": "Kč",
    "label": "Czech Republic Koruna",
    "symbol_native": "Kč",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "CZK",
    'emoji_flag': '🇨🇿',
    "label_plural": "Czech Republic korunas"
  },
  {
    "symbol": "Dkr",
    "label": "Danish Krone",
    "symbol_native": "kr",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "DKK",
    'emoji_flag': '🇩🇰',
    "label_plural": "Danish kroner"
  },
  {
    "symbol": "RD$",
    "label": "Dominican Peso",
    "symbol_native": "RD$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "DOP",
    'emoji_flag': '🇩🇴',
    "label_plural": "Dominican pesos"
  },
  {
    "symbol": "EGP",
    "label": "Egyptian Pound",
    "symbol_native": "ج.م.‏",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "EGP",
    'emoji_flag': '🇪🇬',
    "label_plural": "Egyptian pounds"
  },
  {
    "symbol": "£",
    "label": "British Pound Sterling",
    "symbol_native": "£",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "GBP",
    'emoji_flag': '🇬🇧',
    "label_plural": "British pounds sterling"
  },
  {
    "symbol": "GTQ",
    "label": "Guatemalan Quetzal",
    "symbol_native": "Q",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "GTQ",
    'emoji_flag': '🇬🇹',
    "label_plural": "Guatemalan quetzals"
  },
  {
    "symbol": "HK$",
    "label": "Hong Kong Dollar",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "HKD",
    'emoji_flag': '🇭🇰',
    "label_plural": "Hong Kong dollars"
  },
  {
    "symbol": "kn",
    "label": "Croatian Kuna",
    "symbol_native": "kn",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "HRK",
    'emoji_flag': '🇭🇷',
    "label_plural": "Croatian kunas"
  },
  {
    "symbol": "Ft",
    "label": "Hungarian Forint",
    "symbol_native": "Ft",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "HUF",
    'emoji_flag': '🇭🇺',
    "label_plural": "Hungarian forints"
  },
  {
    "symbol": "Rp",
    "label": "Indonesian Rupiah",
    "symbol_native": "Rp",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "IDR",
    'emoji_flag': '🇮🇩',
    "label_plural": "Indonesian rupiahs"
  },
  {
    "symbol": "₪",
    "label": "Israeli New Sheqel",
    "symbol_native": "₪",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "ILS",
    'emoji_flag': '🇮🇱',
    "label_plural": "Israeli new sheqels"
  },
  {
    "symbol": "Rs",
    "label": "Indian Rupee",
    "symbol_native": "টকা",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "INR",
    'emoji_flag': '🇮🇳',
    "label_plural": "Indian rupees"
  },
  {
    "symbol": "Ikr",
    "label": "Icelandic Króna",
    "symbol_native": "kr",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "ISK",
    'emoji_flag': '🇮🇸',
    "label_plural": "Icelandic krónur"
  },
  {
    "symbol": "¥",
    "label": "Japanese Yen",
    "symbol_native": "￥",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "JPY",
    'emoji_flag': '🇯🇵',
    "label_plural": "Japanese yen"
  },
  {
    "symbol": "₩",
    "label": "South Korean Won",
    "symbol_native": "₩",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "KRW",
    'emoji_flag': '🇰🇷',
    "label_plural": "South Korean won"
  },
  {
    "symbol": "KZT",
    "label": "Kazakhstani Tenge",
    "symbol_native": "тңг.",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "KZT",
    'emoji_flag': '🇰🇿',
    "label_plural": "Kazakhstani tenges"
  },
  {
    "symbol": "RM",
    "label": "Malaysian Ringgit",
    "symbol_native": "RM",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "MYR",
    'emoji_flag': '🇲🇾',
    "label_plural": "Malaysian ringgits"
  },
  {
    "symbol": "Nkr",
    "label": "Norwegian Krone",
    "symbol_native": "kr",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "NOK",
    'emoji_flag': '🇳🇴',
    "label_plural": "Norwegian kroner"
  },
  {
    "symbol": "NZ$",
    "label": "New Zealand Dollar",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "NZD",
    'emoji_flag': '🇳🇿',
    "label_plural": "New Zealand dollars"
  },
  {
    "symbol": "B/.",
    "label": "Panamanian Balboa",
    "symbol_native": "B/.",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "PAB",
    'emoji_flag': '🇵🇦',

    "label_plural": "Panamanian balboas"
  },
  {
    "symbol": "S/.",
    "label": "Peruvian Nuevo Sol",
    "symbol_native": "S/.",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "PEN",
    'emoji_flag': '🇵🇪',
    "label_plural": "Peruvian nuevos soles"
  },
  {
    "symbol": "₱",
    "label": "Philippine Peso",
    "symbol_native": "₱",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "PHP",
    'emoji_flag': '🇵🇭',
    "label_plural": "Philippine pesos"
  },
  {
    "symbol": "PKRs",
    "label": "Pakistani Rupee",
    "symbol_native": "₨",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "PKR",
    'emoji_flag': '🇵🇰',
    "label_plural": "Pakistani rupees"
  },
  {
    "symbol": "zł",
    "label": "Polish Zloty",
    "symbol_native": "zł",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "PLN",
    'emoji_flag': '🇵🇱',
    "label_plural": "Polish zlotys"
  },
  {
    "symbol": "₲",
    "label": "Paraguayan Guarani",
    "symbol_native": "₲",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "PYG",
    'emoji_flag': '🇵🇾',
    "label_plural": "Paraguayan guaranis"
  },
  {
    "symbol": "RON",
    "label": "Romanian Leu",
    "symbol_native": "RON",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "RON",
    'emoji_flag': '🇷🇴',
    "label_plural": "Romanian lei"
  },
  {
    "symbol": "RUB",
    "label": "Russian Ruble",
    "symbol_native": "руб.",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "RUB",
    'emoji_flag': '🇷🇺',
    "label_plural": "Russian rubles"
  },
  {
    "symbol": "SR",
    "label": "Saudi Riyal",
    "symbol_native": "ر.س.‏",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "SAR",
    'emoji_flag': '🇸🇦',
    "label_plural": "Saudi riyals"
  },
  {
    "symbol": "Skr",
    "label": "Swedish Krona",
    "symbol_native": "kr",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "SEK",
    'emoji_flag': '🇸🇪',
    "label_plural": "Swedish kronor"
  },
  {
    "symbol": "S$",
    "label": "Singapore Dollar",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "SGD",
    'emoji_flag': '🇸🇬',
    "label_plural": "Singapore dollars"
  },
  {
    "symbol": "฿",
    "label": "Thai Baht",
    "symbol_native": "฿",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "THB",
    'emoji_flag': '🇹🇭',
    "label_plural": "Thai baht"
  },
  {
    "symbol": "TL",
    "label": "Turkish Lira",
    "symbol_native": "TL",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "TRY",
    'emoji_flag': '🇹🇷',
    "label_plural": "Turkish Lira"
  },
  {
    "symbol": "NT$",
    "label": "New Taiwan Dollar",
    "symbol_native": "NT$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "TWD",
    'emoji_flag': '🇹🇼',
    "label_plural": "New Taiwan dollars"
  },
  {
    "symbol": "₴",
    "label": "Ukrainian Hryvnia",
    "symbol_native": "₴",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "UAH",
    'emoji_flag': '🇺🇦',
    "label_plural": "Ukrainian hryvnias"
  },
  {
    "symbol": "$U",
    "label": "Uruguayan Peso",
    "symbol_native": "$",
    "decimal_digits": 2,
    "rounding": 0,
    "value": "UYU",
    'emoji_flag': '🇺🇾',
    "label_plural": "Uruguayan pesos"
  },
  {
    "symbol": "₫",
    "label": "Vietlabelse Dong",
    "symbol_native": "₫",
    "decimal_digits": 0,
    "rounding": 0,
    "value": "VND",
    "emoji_flag": '🇻🇳',
    "label_plural": "Vietlabelse dong"
  }
];