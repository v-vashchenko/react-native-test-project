import React from 'react';
import { Container, Body, Button, Text, } from 'native-base';
import { View } from 'react-native';
import styles from './styles';

const ButtonsBar = ({ acceptCb, declineCb }) => {
  return (
    <View
      style={styles.buttonContainer}
    >
      <Button
        rounded
        danger
        onPress={() => declineCb()}
        style={styles.decline}
      >
        <Text>Decline</Text>
      </Button>

      <Button
        rounded
        success
        onPress={() => acceptCb()}
        style={styles.accept}
      >
        <Text>Accept</Text>
      </Button>
    </View>
  )
};

export default ButtonsBar;