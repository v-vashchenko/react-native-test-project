import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { padding: 16, paddingTop: 30, backgroundColor: '#fff', justifyContent: "flex-start", alignContent: 'flex-start' },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  row: { flexDirection: 'row', },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'space-around',
  }
});
