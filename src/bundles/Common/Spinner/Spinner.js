import React, { Component } from 'react';
import { Container, Header, Content, Spinner } from 'native-base';

export default class SpinnerOrange extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Spinner color='#F48024' />
        </Content>
      </Container>
    );
  }
}