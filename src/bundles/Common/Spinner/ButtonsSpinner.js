import React, { Component } from 'react';
import { Container, Header, Content, Spinner } from 'native-base';

export default class ButtonsSpinner extends Component {
  render() {
    return (
      <Spinner color='#F48024' />
    );
  }
}