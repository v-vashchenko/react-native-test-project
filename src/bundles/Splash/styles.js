import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff', justifyContent: "center", alignItems: 'center' },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  row: { flexDirection: 'row' }
});
