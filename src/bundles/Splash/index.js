import React, { Component } from 'react'
import {
  Image
} from 'react-native';

import firebase from 'react-native-firebase';

import { Container } from 'native-base';

import connect from './connect';
import styles from './styles';

class Splash extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null
    };
  }

  constructor() {
    super();
    this.unsubscriber = null;
  }

  async componentDidMount() {
    this.unsubscriber = await firebase.auth().onAuthStateChanged(async (user) => {
      console.log('@@@user', user);
      if (!user) {
        this.props.navigation.navigate('Login')
      } else {
        const { email, uid } = user;
        console.log('@@@email', email);
        console.log('@@@uid', uid);
        await this.props.loginStatus(true, { email, uid });
        this.props.navigation.replace('RateList')
      }
    })

  }

  componentWillUnmount() {
  }

  render() {
    return (
      <Container
        style={styles.container}>
        <Image
          source={require('../../static/images/logo_splash.png')}
        />
      </Container   >
    )
  }
}

export default connect(Splash);