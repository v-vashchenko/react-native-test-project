import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff', zIndex: 1, },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  row: { flexDirection: 'row' },
  paginationButtonsWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  spinner: {
    width: 50,
    height: 50,
  },
  spinnerContainer: { flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' },
  card: {
    height: 50,
    borderWidth: 2,
    borderColor: '#f1f8ff',
    borderStyle: 'solid',
    marginBottom: 10,
    borderRadius: 4,
    alignItems: 'center',
    padding: 10
  },
  currency: {
    marginRight: 'auto',
    marginLeft: 0
  },
  rate: {
    marginLeft: 'auto',
    marginRight: 0
  },
  picker: {
    height: 50,
    marginBottom: 10,
  },
  converterString: {
    height: 50,
    marginBottom: 10,
    alignItems: 'center',
    borderRadius: 5,
    backgroundColor: '#f1f8ff',
  },
  input: {
    flex: 2,
    paddingLeft: 60
  },
  currency: {
    zIndex: 100,
    marginBottom: 10,
    marginRight: 5,
    backgroundColor: '#F48024',
    color: 'white',
    borderRadius: 5,
    height: 50,
    width: 50,
    textAlign: 'center',
    textAlignVertical: 'center'
  },
  result: {
    flex: 3,
    textAlign: 'right',
    padding: 15
  },
  buttonLabel: {
    color: 'white',
  },
  revert: {
    display: 'flex',
    height: 40,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  labelsContainer: {
    position: 'relative',
    top: -160,
    width: 50
  }
});
