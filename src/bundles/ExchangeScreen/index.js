import React, { Component } from 'react'
import {
  Text,
  Picker,
  TextInput,
  Clipboard,
  Animated,
} from 'react-native';

import { Button, Icon, View, Toast } from 'native-base';

import connect from './connect';
import { NavigationHeader } from '../Common/NavigationHeader';
import styles from './styles';

import { CURRENCIES_LIST } from '../Core/constants';
import { TouchableOpacity } from 'react-native-gesture-handler';

class ExchangeScreen extends Component {

  state = {
    order: ['baseCurrency', 'targetCurrency'],
    baseAmount: '',
    targetAmount: '',
    SlideInX: new Animated.Value(0),
    SlideInY: new Animated.Value(0),
    fadeIn: new Animated.Value(0),
    smaller: new Animated.Value(0),
    bigger: new Animated.Value(0),
    swapUp: new Animated.Value(0),
    swapLeft: new Animated.Value(0),
    swapDown: new Animated.Value(0),
    swapRight: new Animated.Value(0)
  }

  componentDidMount() {
    this._start();
  }

  _start = () => {
    return Animated.parallel([
      Animated.timing(this.state.SlideInX, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }),

      Animated.timing(this.state.SlideInY, {
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }),
      Animated.timing(this.state.fadeIn, {
        toValue: 1,
        duration: 600,
        useNativeDriver: true
      }),

    ]).start();
  };

  _swapUp = () => {
    const { order } = this.state;
    const way = order[0] === 'baseCurrency';
    return Animated.parallel([
      Animated.sequence([
        Animated.timing(this.state.smaller, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }),

        Animated.timing(this.state.smaller, {
          toValue: 0,
          duration: 500,
          useNativeDriver: true
        })]),

      Animated.sequence([
        Animated.timing(this.state.swapLeft, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }),

        Animated.timing(this.state.swapLeft, {
          toValue: 0,
          duration: 500,
          useNativeDriver: true
        })]),

      Animated.timing(this.state.swapUp, {
        toValue: !way ? 0 : 1,
        duration: 500,
        useNativeDriver: true
      })
    ]).start()
  }

  _swapDown = () => {
    const { order } = this.state;
    const way = order[0] === 'baseCurrency';
    return Animated.parallel([
      Animated.sequence([
        Animated.timing(this.state.bigger, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }),

        Animated.timing(this.state.bigger, {
          toValue: 0,
          duration: 500,
          useNativeDriver: true
        })]),

      Animated.sequence([
        Animated.timing(this.state.swapRight, {
          toValue: 1,
          duration: 500,
          useNativeDriver: true
        }),

        Animated.timing(this.state.swapRight, {
          toValue: 0,
          duration: 500,
          useNativeDriver: true
        })]),

      Animated.timing(this.state.swapDown, {
        toValue: way ? 1 : 0,
        duration: 500,
        useNativeDriver: true
      })
    ]).start()
  }

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <NavigationHeader
          title='Exchanger'
          navigation={navigation}
          showBackButton
        />
      ),
    };
  }

  changeBaseCurrency = itemValue => {
    this.props.getRates(itemValue);
  }

  changeTargetCurrency = itemValue => {
    this.props.changeTargetCurrency(itemValue);
  }

  mapPickerItems = () => {
    return CURRENCIES_LIST.map((el, index) => {
      const { label, value } = el;
      if (label && value) {
        const key = index + value + label;
        return <Picker.Item label={label} value={value} key={key} />
      }
    })
  }

  onChangeText = (text) => {
    text = text.trim();
    this.setState({ searchString: text });
  }

  onReverse = () => {
    this._swapDown();
    this._swapUp();
    let { order, baseAmount, targetAmount } = this.state;
    order = order.reverse();
    this.setState({ order, baseAmount: targetAmount, targetAmount: baseAmount });
  }

  onChangeBaseAmount = (text) => {
    const previousAmount = { baseAmount: this.state.baseAmount, targetAmount: this.state.targetAmount };
    let baseAmount = +text;
    let targetAmount;
    const pattern = /^\d{0,12}(\.\d{0,2})?$/;
    if (pattern.test(text)) {
      const { rates, targetCurrency } = this.props;
      const { order } = this.state;
      const way = order[0] === 'baseCurrency';
      targetAmount = way ? baseAmount * rates[targetCurrency] : baseAmount / rates[targetCurrency];
      targetAmount = targetAmount.toFixed(2);
      baseAmount = baseAmount.toString();
      if (text[text.length - 1] === '.') {
        baseAmount += '.';
      }
      targetAmount = targetAmount.toString();
      this.setState({ baseAmount, targetAmount });
    } else {
      this.setState(previousAmount);
    }
  }

  copyToClipboard = async () => {
    const { targetAmount } = this.state;
    if (targetAmount) {
      Toast.show({
        text: 'Copied to clipboard!',
        buttonText: 'ok',
        duration: 2000
      })
      await Clipboard.setString(this.state.targetAmount);
    }
  }

  render() {
    const { SlideInX, SlideInY, smaller, bigger, swapUp, swapDown, swapLeft, swapRight, fadeIn } = this.state;
    return (
      <View style={styles.container}>
        <Animated.View style={[styles.converterString, styles.row, {
          transform: [
            {
              translateX: SlideInX.interpolate({
                inputRange: [0, 1],
                outputRange: [-600, 0]
              })
            },
          ]
        }]}
        >
          <TextInput
            style={styles.input}
            placeholder='Enter amount (f.e. "222.11")'
            value={this.state.baseAmount}
            onChangeText={this.onChangeBaseAmount}
            keyboardType='numeric'
          />

        </Animated.View>

        <Animated.View
          style={{
            transform: [
              {
                translateX: SlideInX.interpolate({
                  inputRange: [0, 1],
                  outputRange: [600, 0]
                })
              }
            ]
          }}
        >
          <TouchableOpacity
            style={[styles.converterString, styles.row]}
            onPress={this.copyToClipboard}
          >
            <Text style={styles.result}>{this.state.targetAmount}</Text>
          </TouchableOpacity>
        </Animated.View>

        <Animated.View style={{
          transform: [
            {
              translateY: SlideInY.interpolate({
                inputRange: [0, 1],
                outputRange: [600, 0]
              })
            }
          ]
        }}>
          <Button
            style={styles.revert}
            onPress={this.onReverse}
          >
            <Text style={styles.buttonLabel}>Revese</Text>
            <Icon type="FontAwesome" name="sort" />
          </Button>
        </Animated.View>

        <View style={styles.labelsContainer}>
          <Animated.View style={[
            {
              opacity: fadeIn,
              transform: [
                {
                  scale: fadeIn.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 1],
                  })
                },
                {
                  scaleX: smaller.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 0.8]
                  })
                },
                {
                  scaleY: smaller.interpolate({
                    inputRange: [0, 1],
                    outputRange: [1, 0.8]
                  })
                },
                {
                  translateX: swapRight.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 10]
                  })
                },
                {
                  translateX: swapRight.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 10]
                  })
                },
                {
                  translateY: swapDown.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, 60]
                  })
                }
              ]
            }
          ]}
          >
            <Text style={styles.currency}>
              {this.props.baseCurrency}
            </Text>
          </Animated.View>

          <Animated.View style={[{
            opacity: fadeIn,
            transform: [
              {
                scale: fadeIn.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 1],
                })
              },
              {
                scaleX: smaller.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 0.8]
                })
              },
              {
                scaleY: smaller.interpolate({
                  inputRange: [0, 1],
                  outputRange: [1, 0.8]
                })
              },
              {
                translateX: swapLeft.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, -20]
                })
              },
              {
                translateX: swapLeft.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, -20]
                })
              },
              {
                translateY: swapUp.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, -60]
                })
              },
            ]
          }]}
          >
            <Text style={styles.currency}>
              {this.props.targetCurrency}
            </Text>

          </Animated.View>
        </View>
      </View >
    )
  }
}

export default connect(ExchangeScreen);