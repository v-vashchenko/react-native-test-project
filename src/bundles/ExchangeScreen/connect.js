import { connect } from 'react-redux';

import * as currenciesActions from '../../store/currencies/currencies.actions';

const mapStateToProps = ({ currencies }) => ({
  baseCurrency: currencies.baseCurrency,
  rates: currencies.rates,
  targetCurrency: currencies.targetCurrency,
});

const mapDispatchToProps = {
  ...currenciesActions,
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
