import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff' },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  row: { flexDirection: 'row', },
  paginationButtonsWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  spinner: {
    width: 50,
    height: 50,
  },
  spinnerContainer: { flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center' },
  card: {
    height: 50,
    borderWidth: 2,
    borderColor: '#f1f8ff',
    borderStyle: 'solid',
    marginBottom: 10,
    borderRadius: 4,
    alignItems: 'center',
    padding: 10
  },
  currency: {
    marginRight: 'auto',
    marginLeft: 0
  },
  rate: {
    marginLeft: 'auto',
    marginRight: 0
  },
  picker: {
    height: 50,
    marginBottom: 10,
  },
  searchInput: {
    flex: 4,
    borderWidth: 2,
    borderStyle: 'solid',
    borderColor: '#f1f8ff',
    borderRadius: 4,
    borderRightWidth: 0,
    paddingLeft: 15
  },
  searchButton: {
    textAlign: 'center',
    paddingLeft: 5,
    justifyContent: 'center',
    height: 37,
    width: 37,
    padding: 0
  },
  search: {
    height: 38,
    marginBottom: 10
  },
  buttonLabel: {
    color: 'white',
  }
});
