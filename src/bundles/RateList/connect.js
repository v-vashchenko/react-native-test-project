import { connect } from 'react-redux';

import * as currenciesActions from '../../store/currencies/currencies.actions';

const mapStateToProps = ({ currencies }) => ({
  baseCurrency: currencies.baseCurrency,
  rates: currencies.rates,
  loadingData: currencies.loadingData
});

const mapDispatchToProps = {
  ...currenciesActions,
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
