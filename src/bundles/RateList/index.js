import React, { Component } from 'react'
import {
  Text,
  Picker,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';

import { View, Button, Icon, Fab, Container, Footer, FooterTab } from 'native-base';

import connect from './connect';
import { NavigationHeader } from '../Common/NavigationHeader';
import styles from './styles';
import { leftTrim } from '../utils/index'

import { CURRENCIES_LIST } from '../Core/constants';
import Spinner from '../Common/Spinner/Spinner';

class RateList extends Component {

  constructor(props) {
    super(props);

    this.spinValue = new Animated.Value(0);

    this.state = {
      searchString: '',
      list: CURRENCIES_LIST,
      goToTop: false,
    }
  };

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <NavigationHeader
          title='Rates'
        />
      ),
    };
  }

  async componentDidMount() {
    this.props.changeLodaingDataStatusCurrencies();
    this.props.getRates();
  }

  spin() {
    console.log('@@@here');
    this.spinValue.setValue(0)
    Animated.timing(
      this.spinValue,
      {
        toValue: 1,
        duration: 1000,
        easing: Easing.linear,
        useNativeDriver: true,
      }
    ).start()
  }

  changeBaseCurrency = itemValue => {
    this.props.getRates(itemValue);
  }

  mapPickerItems = () => {
    return CURRENCIES_LIST.map((el, index) => {
      const { label, value } = el;
      if (label && value) {
        const key = index + value + label;
        return <Picker.Item label={label} value={value} key={key} />
      }
    })
  }

  mapRateItems = () => {
    const { list } = this.state;
    return list.map((el, index) => {
      const { label, value, emoji_flag } = el;
      const key = index + label + value;
      const { rates } = this.props;
      if (el.value !== this.props.baseCurrency) {
        return (
          <TouchableOpacity
            key={key}
            style={[styles.card, styles.row]}
            onPress={() => this.onCardPress(el.value)}
          >
            <Text style={styles.currency}>
              {emoji_flag} {label} ({value})
            </Text>
            <Text style={styles.rate}>
              {(1 / rates[value]).toFixed(4)}
            </Text>
          </TouchableOpacity>
        )
      }
    })
  }

  onCardPress = (targetCurrency) => {
    if (targetCurrency) {
      this.props.changeTargetCurrency(targetCurrency);
      this.props.navigation.push('ExchangeScreen')
    }
  }

  onChangeText = async (text) => {
    let leftTrimmedText = leftTrim(text);
    await this.setState({ searchString: leftTrimmedText });
    if (text === '') {
      this.onSearchSubmit();
    }
  }

  onSearchSubmit = () => {
    this.spin();
    const { searchString, list } = this.state;
    if (searchString) {
      filtered = list.filter(el => el.value.toLowerCase().includes(searchString.toLowerCase())
        || el.label.toLowerCase().includes(searchString.toLowerCase()));
      this.setState({ list: filtered });
    } else {
      this.setState({ list: CURRENCIES_LIST })
    }
  }

  onScrollHandler = (event) => {
    const { goToTop } = this.state;
    if (!goToTop && event.nativeEvent.contentOffset.y > 200) {
      this.setState({ goToTop: true });
    } else if (goToTop && event.nativeEvent.contentOffset.y < 200) {
      this.setState({ goToTop: false });
    }
  }

  goTop = () => {
    this.listView.scrollTo({ y: 0, animated: true });
    this.setState({ goToTop: false })
  }

  onProfile = () => {
    this.props.navigation.push('Profile')
  }

  onBooks = () => {
    this.props.navigation.push('CategoriesList')
  }

  onTable = () => {
    this.props.navigation.push('TableScreen');
  }

  render() {
    const { goToTop } = this.state;
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    })

    if (this.props.loadingData) {
      return <Spinner />
    }
    return (
      <Container style={styles.container}>
        <Text>Base currency: </Text>
        <Picker
          style={styles.picker}
          onValueChange={this.changeBaseCurrency}
          selectedValue={this.props.baseCurrency}
        >
          {this.mapPickerItems()}
        </Picker>
        <View style={[styles.search, styles.row]}>
          <TextInput
            style={styles.searchInput}
            value={this.state.searchString}
            onChangeText={this.onChangeText}
            placeholder='Search by title or code'
          />
          <Button
            style={styles.searchButton}
            onPress={this.onSearchSubmit}
          >
            <Animated.View
              style={{ transform: [{ rotate: spin }], width: 60, height: 25 }}
            >

              <Icon
                type="FontAwesome"
                name="search"
              />
            </Animated.View>
          </Button>
        </View>
        <ScrollView
          onScroll={this.onScrollHandler}
          ref={ref => this.listView = ref}
        >
          {this.mapRateItems()}
        </ScrollView>
        {
          goToTop &&
          <View>
            <Fab
              active={true}
              direction="up"
              style={{ backgroundColor: '#5067FF' }}
              position="bottomLeft"
              onPress={this.goTop}
            >
              <Icon
                type="FontAwesome"
                name="arrow-up"
              />
            </Fab>
          </View>
        }
        <Footer>
          <FooterTab>
            <Button
              onPress={this.onProfile}
            >
              <Icon
                type="FontAwesome"
                name="user"
              />
            </Button>
            <Button active>
              <Icon
                active
                type="FontAwesome"
                name="calculator"
              />
            </Button>
            <Button
              onPress={this.onBooks}
            >
              <Icon
                type="FontAwesome"
                name="book"
              />
            </Button>
            <Button
              onPress={this.onTable}
            >
              <Icon
                type="FontAwesome"
                name="table"
              />
            </Button>
          </FooterTab>
        </Footer>
      </Container >
    )
  }
}

export default connect(RateList);