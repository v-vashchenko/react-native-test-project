import React, { Component } from 'react'
import { NavigationHeader } from '../Common/NavigationHeader';
import ImagePicker from 'react-native-image-crop-picker';
import moment from 'moment'

import { Image, TouchableOpacity } from 'react-native'
import { Container, Text, Icon, Form, Item, Input, DatePicker, Content, View, Card, CardItem, Body } from 'native-base';
import BooksList from '../BooksList';

import connect from './connect';

import Spinner from '../Common/Spinner/Spinner'
import styles from './styles';

import ButtonsBar from '../Common/ConfirmButtonsBar'

import { leftTrim } from '../utils/index';
import { ScrollView } from 'react-native-gesture-handler';


class Profile extends Component {

  constructor(props) {
    super(props);

    let { fullName = null, DoB = null } = this.props.globalUser;

    if (DoB) {
      DoB = DoB.toDate();
    }

    this.state = {
      DoB,
      fullName,
      updated: false
    }
  };

  async componentWillMount() {
    if (!this.props.globalUser) {
      await this.props.uptateGlobalUser()
    }
  }

  async componentDidMount() {
    await this.props.navigation.setParams({ logOut: this._onLogOut });
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      header: (
        <NavigationHeader
          {...params}
          title='Profile'
          navigation={navigation}
          showBackButton
          showLogOut={true}
          logOut={navigation.getParam('logOut')}
        />
      ),
    };
  }

  _onLogOut = async () => {
    await this.props.navigation.replace('Login');
    this.props.logOut();
  }

  onImagePress = () => {
    try {
      ImagePicker.openPicker({
        width: 300,
        height: 400,
      }).then(async image => {
        this.props.updateProfileImage(this.props.globalUser, image.path);
      });
    } catch (err) {
      console.log('@@@err', err);
    }
  }

  onChange = () => {
    const { updated } = this.state;
    if (!updated) this.setState({ updated: true });
  }

  onTextChange = (text, field) => {
    this.onChange();
    let leftTrimmedText = leftTrim(text);
    this.setState({ [field]: leftTrimmedText })
  }

  onDateChange = (date, field) => {
    this.onChange();
    this.setState({ [field]: date });
  }

  onAccept = async () => {
    this.props.changeLodaingDataStatus();
    await this.props.updateProfileData(this.props.globalUser, this.state);
    this.setState({ updated: false });
  }

  onDecline = () => {
    let DoB, fullName;
    if (this.props.globalUser) {
      DoB = this.props.globalUser.DoB;
      fullName = this.props.globalUser.fullName;
    }
    this.setState({
      DoB,
      fullName,
      updated: false
    })
  }

  onCardPress = async (bookData) => {
    this.props.changeBooksLodaingDataStatus()
    await this.props.selectBook(bookData);
    this.props.navigation.navigate('BookScreen');
  }

  render() {
    let { profilePicture = '' } = this.props.globalUser;
    let { starredBooks = [] } = this.props.globalUser;

    const source = profilePicture ? { uri: profilePicture } : require('../../static/images/default_user.jpg');

    if (this.props.loadingData) {
      return <Spinner />
    }

    const { fullName, DoB } = this.state;

    return (
      <ScrollView>
        <View style={styles.content}>
          <TouchableOpacity
            onPress={this.onImagePress}
            style={styles.avatarContainer}
          >
            <Image
              source={source}
              style={styles.avatar}
            />
          </TouchableOpacity>
          <Form style={styles.form}>
            <Item
              rounded
              inlineLabel
              style={styles.item}>
              <Input
                value={fullName || null}
                placeholder='Full Name'
                onChangeText={(text) => this.onTextChange(text, 'fullName')}
              />
            </Item>
            <Item
              rounded
              inlineLabel
              style={styles.item}
            >
              <DatePicker
                defaultDate={DoB || null}
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                textStyle={{ color: "green" }}
                onDateChange={(date) => this.onDateChange(date, 'DoB')}
                disabled={false}
                placeHolderTextStyle={styles.datePicker}
                placeHolderText={DoB ? null : 'Select Date of Birth'}
              />
            </Item>
          </Form>
        </View>
        {
          this.state.updated
            ? <ButtonsBar
              acceptCb={this.onAccept}
              declineCb={this.onDecline}
            />
            : null
        }
        <Text
          style={styles.item}
        >
          {starredBooks.length
            ? 'Starred Books'
            : 'There is no starred books!'}
        </Text>
        {starredBooks.map((el, index) => {
          const { author, book_image, description, title } = el;
          return (
            <Card
              key={title + index}
            >
              <CardItem
                button
                onPress={() => this.onCardPress(el)}
              >
                <Image
                  source={{ uri: book_image }}
                  style={styles.preview}
                />
                <Body>
                  <Text style={styles.title}
                  >
                    {title}
                  </Text>
                  <Text style={styles.subtitle}
                  >
                    by {author}
                  </Text>
                  <Text style={styles.description}
                  >
                    {description}
                  </Text>
                </Body>
              </CardItem>
            </Card>
          )
        })}
      </ScrollView>

    )
  }
}

export default connect(Profile);