import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  content: { flex: 1, paddingTop: 10, paddingLeft: 5, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start', marginBottom: 15 },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  avatar: { width: 100, height: 100, borderRadius: 50 },
  avatarContainer: {
  },
  form: {
    paddingRight: 10,
  },
  item: {
    paddingLeft: 15,
    minWidth: 200,
    height: 50,
    marginBottom: 5,
  },
  input: {
  },
  label: {
    color: '#000'
  },
  datePicker: {
    paddingLeft: 0
  },
  preview: {
    width: 50,
    height: 70,
    marginRight: 7
  },
  title: {
    fontWeight: 'bold',
    fontSize: 8
  },
  subtitle: {
    fontSize: 5
  },
  description: {
    fontSize: 8
  }
});
