import { connect } from 'react-redux';

import * as userActions from '../../store/user/user.actions';
import * as booksActions from '../../store/books/books.actions';

const mapStateToProps = ({ user }) => ({
  globalUser: user.globalUser,
  isLogged: user.isLogged,
  loadingData: user.loadingData
});

const mapDispatchToProps = {
  ...userActions,
  ...booksActions
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
