import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff', flexDirection: 'column' },
  button: {
    flex: 1,
    marginTop: 10,
    alignSelf: 'stretch',
    textAlign: 'center',
    justifyContent: 'center'
  },
  button_title: {
    color: '#ffffff'
  }
});
