import React, { Component } from 'react';
import { Text, Button, Container, Content, Form, Item, Input } from 'native-base';
import connect from './connect';
import { NavigationHeader } from '../../Common/NavigationHeader';
import styles from './styles';
import { emailRegex } from '../../Core/constants'
import Spinner from '../../Common/Spinner/ButtonsSpinner'
import ErrorTextBlock from '../ErrorTextBlock';
import { Keyboard } from 'react-native';
import propTypes from 'prop-types';


class Restore extends Component {

  state = {
    email: '',
    errorObject: {}
  };

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <NavigationHeader
          title='Forgot password?'
          navigation={navigation}
          showBackButton
        />
      ),
    };
  }

  onChangeText = (text, type) => {
    text = text.trim();
    this.setState({ [type]: text, errorObject: {} });
  }

  onSubmit = async () => {
    Keyboard.dismiss();
    let { email, errorObject } = this.state;
    if (emailRegex.test(email)) {
      this.props.changeLodaingDataStatus();
      await this.props.restorePassword(email);
      this.props.navigation.push('Login');
      if (this.props.error) {
        email = ''
        errorObject.message = this.props.error;
      }
    } else {
      errorObject.email = true;
      this.setState({ errorObject, email });
    }
  }

  render() {
    const { errorObject } = this.state;
    if (this.props.loadingData) {
      return <Spinner />
    }
    return (
      <Container style={styles.container}>
        <Content>
          <Form>
            <Item last>
              <Input
                placeholder="E-mail"
                value={this.state.email}
                onChangeText={(text) => this.onChangeText(text, 'email')}
              />
            </Item>
            <Button
              primary
              full
              style={styles.button}
              onPress={this.onSubmit}
            >
              {
                this.props.loadingData
                  ? <Spinner />
                  : <Text style={styles.button_title}>Restore password</Text>
              }
            </Button>
          </Form>
        </Content>
        <ErrorTextBlock
          data={errorObject}
        />
      </Container >
    )
  }
}

Restore.propTypes = {
  changeLodaingDataStatus: propTypes.func.isRequired,
  restorePassword: propTypes.func.isRequired,
  globalUser: propTypes.shape({ email: propTypes.string }),
  loadingData: propTypes.bool.isRequired,
  navigation: propTypes.shape().isRequired,
  error: propTypes.string
};

Restore.defaultProps = {
  globalUser: { email: null },
  error: null
};

export default connect(Restore);