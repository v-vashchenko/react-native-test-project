import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { padding: 16, paddingTop: 10, backgroundColor: '#fff', flexDirection: 'column' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  row: { flexDirection: 'row', },
  title: {
    textAlign: 'center',
  },
  error: {
    textAlign: 'left',
    color: 'red'
  },
  textBlock: {
    marginBottom: 10
  }
});
