import { connect } from 'react-redux';

import * as userActions from '../../../store/user/user.actions';

const mapStateToProps = ({ user }) => ({
  globalUser: user.globalUser,
  loadingData: user.loadingData,
  error: user.error,
});

const mapDispatchToProps = {
  ...userActions,
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
