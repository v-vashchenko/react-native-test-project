import React, { Component } from 'react'
import { Button, Container, Content, Form, Item, Input, Text } from 'native-base';
import connect from './connect';
import { NavigationHeader } from '../../Common/NavigationHeader';
import styles from './styles';
import { emailRegex } from '../../Core/constants'
import Spinner from '../../Common/Spinner/ButtonsSpinner'
import ErrorTextBlock from '../ErrorTextBlock';
import { Keyboard } from 'react-native';
import propTypes from 'prop-types';


class SignUp extends Component {

  state = {
    email: '',
    password: '',
    errorObject: {}
  };

  static navigationOptions = ({ navigation }) => {
    return {
      header: (
        <NavigationHeader
          title='SignUp'
        />
      ),
    };
  }

  onChangeText = (text, type) => {
    text = text.trim();
    this.setState({ [type]: text, errorObject: {} });
  }

  onLogin = () => {
    if (!this.props.loadingData) {
      this.props.navigation.push('Login')
    }
  }

  onSubmit = async () => {
    Keyboard.dismiss();
    let { email, password, errorObject } = this.state;
    const passwordRegex = /.{6,}/;
    const isValidEmail = emailRegex.test(email);
    const isValidPassword = passwordRegex.test(password);
    if (!isValidEmail) {
      errorObject.email = true;
    }
    else if (!isValidPassword) {
      errorObject.password = true;
    }
    else if (isValidEmail && isValidPassword) {
      this.props.changeLodaingDataStatus();
      await this.props.signUp(email, password);
      if (this.props.globalUser) {
        this.props.navigation.push('RateList')
      } else if (this.props.error) {
        email = '';
        password = '';
        errorObject.message = this.props.error;
      }
    }
    this.setState({ errorObject, email, password });
  }

  render() {
    const { errorObject } = this.state;
    return (
      <Container style={styles.container}>
        <Content>
          <Form>
            <Item>
              <Input
                placeholder="E-mail"
                value={this.state.email}
                onChangeText={(text) => this.onChangeText(text, 'email')}
              />
            </Item>
            <Item last>
              <Input
                secureTextEntry
                placeholder="Password"
                value={this.state.password}
                onChangeText={(text) => this.onChangeText(text, 'password')}
              />
            </Item>
            <Button
              primary
              full
              style={styles.submit}
              onPress={this.onSubmit}
            >
              {
                this.props.loadingData
                  ? <Spinner />
                  : <Text style={styles.button_title}>SIGN UP</Text>
              }
            </Button>
            <Button
              light
              style={styles.submit}
              onPress={this.onLogin}
            >
              <Text style={styles.button_title_black}>Already have an account?</Text>
            </Button>
          </Form>
          <ErrorTextBlock
            data={errorObject}
          />
        </Content>
      </Container >
    )
  }
}

SignUp.propTypes = {
  changeLodaingDataStatus: propTypes.func.isRequired,
  signUp: propTypes.func.isRequired,
  globalUser: propTypes.shape({ email: propTypes.string }),
  loadingData: propTypes.bool.isRequired,
  navigation: propTypes.shape().isRequired,
  error: propTypes.string
}

SignUp.defaultProps = {
  globalUser: { email: null },
  error: null
}

export default connect(SignUp);