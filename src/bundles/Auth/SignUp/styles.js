import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff', flexDirection: 'column' },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },
  row: { flexDirection: 'row', },
  submit: {
    flex: 1,
    marginTop: 10,
    alignSelf: 'stretch',
    textAlign: 'center',
    justifyContent: 'center'
  },
  button_title: {
    color: '#ffffff'
  },
  button_title_black: {
    color: '#000000'
  }
});
