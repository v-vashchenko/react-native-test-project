import React from 'react';
import { Content, Text } from 'native-base';
import styles from './styles';

const ErrorTextBlock = ({ data }) => {
  const { email, password, message } = data;
  if (!email && !password && !message) {
    return null;
  }
  return (
    <Content style={styles.container}>
      <Text style={[styles.title, styles.textBlock]}>
        Something went wrong :(
            </Text>
      {
        email ?
          <Text style={[styles.error, styles.textBlock]}>
            E-mail is invalid, valid example: "example.example@example.com"!
            </Text>
          : null
      }
      {
        password ?
          <Text style={[styles.error, styles.textBlock]}>
            Password should contain at least 6 characters!
            </Text>
          : null
      }
      {
        message ?
          <Text style={[styles.error, styles.textBlock]}>
            {message}
          </Text>
          : null
      }

    </Content>
  )
}

export default ErrorTextBlock;