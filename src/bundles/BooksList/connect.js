import { connect } from 'react-redux';

import * as booksActions from '../../store/books/books.actions';

const mapStateToProps = ({ books }) => ({
  booksList: books.booksList,
  loadingData: books.loadingData,
  currentCategory: books.currentCategory,
  currentCategoryEncoded: books.currentCategoryEncoded,
  count: books.count
});

const mapDispatchToProps = {
  ...booksActions,
};

export default (container) => connect(mapStateToProps, mapDispatchToProps)(container);
