import React, { Component } from 'react'
import { NavigationHeader } from '../Common/NavigationHeader';
import moment from 'moment';
import propTypes from 'prop-types';

import { Image, TouchableOpacity } from 'react-native'
import { Container, Text, Icon, Form, Item, Input, DatePicker, Content, View, Card, CardItem, Body } from 'native-base';

import connect from './connect';

import Spinner from '../Common/Spinner/Spinner'
import styles from './styles';

import { leftTrim } from '../utils/index';


class BooksList extends Component {

  async componentDidMount() {
    this.props.navigation.setParams({ title: this.props.currentCategory || 'Books' });
    if (!this.props.booksList.length && this.props.currentCategory) {
      this.props.changeBooksLodaingDataStatus();
      await this.props.getBooks(this.props.currentCategoryEncoded);
    }
  }

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    return {
      header: (
        <NavigationHeader
          {...params}
          navigation={navigation}
          showBackButton
        />
      ),
    };
  }

  onCardPress = async (bookData) => {
    this.props.changeBooksLodaingDataStatus()
    await this.props.selectBook(bookData);
    this.props.navigation.navigate('BookScreen');
  }

  render() {

    if (this.props.loadingData) {
      return <Spinner />
    }

    const { booksList = [] } = this.props;

    return (
      <Container>
        <Content>
          {booksList.map((el, index) => {
            const { author, book_image, description, title } = el;
            return (
              <Card
                key={title + index}
              >
                <CardItem
                  button
                  onPress={() => this.onCardPress(el)}
                >
                  <Image
                    source={{ uri: book_image }}
                    style={styles.preview}
                  />
                  <Body>
                    <Text style={styles.title}
                    >
                      {title}
                    </Text>
                    <Text style={styles.subtitle}
                    >
                      by {author}
                    </Text>
                    <Text style={styles.description}
                    >
                      {description}
                    </Text>
                  </Body>
                </CardItem>
              </Card>
            )
          })}
        </Content>
      </Container>
    )
  }
}

BooksList.propTypes = {
  booksList: propTypes.arrayOf(propTypes.shape())
};

BooksList.defaultProps = {
  booksList: []
};

export default connect(BooksList);