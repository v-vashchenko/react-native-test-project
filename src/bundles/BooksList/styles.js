import { StyleSheet, Platform } from 'react-native';

export default StyleSheet.create({
  paginationButtonsWrapper: {
    // backgroundColor: '#4FC08D',
  },
  content: { flex: 1, paddingTop: 10, paddingLeft: 5, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between', marginBottom: 15 },
  head: { backgroundColor: '#f1f8ff' },
  text: { margin: 6, textAlign: 'center', flexWrap: 'nowrap', fontSize: 10, },

  preview: {
    width: 50,
    height: 70,
    marginRight: 7
  },
  title: {
    fontWeight: 'bold',
    fontSize: 8
  },
  subtitle: {
    fontSize: 5
  },
  description: {
    fontSize: 8
  }
});
