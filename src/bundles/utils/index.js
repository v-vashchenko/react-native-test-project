export const leftTrim = (string) => {
  if (!string) return string;
  return string.replace(/^\s+/g, '');
}