import firebase from 'react-native-firebase';

const db = firebase.firestore();

export const _createFirestoreUserRecord = async (data) => {
  try {
    return await db.collection('user_data').doc(data.id).set(data);
  } catch (err) {
    throw new Error('Error creating dbRecord:', err)
  }
}

export const _readFirestoreUserRecord = async (uid) => {
  console.log('@@@READING DB RECORD');
  let docRef = db.collection("user_data").doc(uid);
  try {
    return docRef.get()
      .then(function (doc) {
        if (doc.exists) {
          console.log('@@@ READING DB RECORD SUCCESS');
          return doc.data();
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
  } catch (err) {
    console.log('@@@err', err);
  }

}

export const _updateFirestoreRecord = async (uid, data) => {
  console.log('@@@uid', uid);
  console.log('@@@data', data);
  try {
    let docRef = db.collection("user_data").doc(uid);
    await docRef.update(data);
  } catch (error) {
    console.log('@@@Error updating DB record: ', error);
  }
}