const initialState = {
  globalUser: null,
  loadingData: false,
  isLogged: null,
  error: null
};

export const user = (state = initialState, action) => {
  console.log('@@@USER_REDUCER_STATE', state);
  switch (action.type) {
    case 'LOG_IN':
      return {
        ...state,
        globalUser: action.globalUser,
        isLogged: action.status,
        loadingData: false,
        error: action.error
      }

    case 'SIGN_UP':
      return {
        ...state,
        globalUser: action.globalUser,
        isLogged: action.status,
        error: action.error,
        loadingData: false,
      }

    case 'LOG_OUT': {
      return {
        ...state,
        globalUser: null,
        loadingData: false
      }
    }
    case 'IS_LOGGED': {
      return {
        ...state,
        isLogged: action.status,
        globalUser: action.globalUser,
        loadingData: false
      }
    }

    case 'CHANGE_LOADING_SHEDULES_DATA_STATUS': {
      return {
        ...state,
        loadingData: true,
      }
    }

    case 'RESTORE_PASSWORD': {
      return {
        ...state,
        loadingData: false,
        error: action.error
      }
    }

    case 'UPDATE_IMAGE': {
      return {
        ...state,
        globalUser: action.globalUser,
        loadingData: false
      }
    }

    case 'UPDATE_DATA': {
      return {
        ...state,
        globalUser: action.globalUser,
        loadingData: false
      }
    }

    default:
      return state;
  };
};