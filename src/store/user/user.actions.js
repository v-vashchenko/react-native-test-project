import firebase from 'react-native-firebase';
import { _createFirestoreUserRecord, _readFirestoreUserRecord, _updateFirestoreRecord } from '../../api/FirestoreActions';

const CHANGE_LOADING_SHEDULES_DATA_STATUS = 'CHANGE_LOADING_SHEDULES_DATA_STATUS';
const IS_LOGGED = 'IS_LOGGED';
const SIGN_UP = 'SIGN_UP';
const LOG_IN = 'LOG_IN';
const LOG_OUT = 'LOG_OUT';
const RESTORE_PASSWORD = 'RESTORE_PASSWORD';
const UPDATE_IMAGE = 'UPDATE_IMAGE';
const UPDATE_DATA = 'UPDATE_DATA';

// change loading status for displaying or not displaying loading spinner
export const changeLodaingDataStatus = () => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_LOADING_SHEDULES_DATA_STATUS,
    })
  }
}

export const updateProfileImage = (globalUser, image) => {
  const { id } = globalUser;
  let { path: oldPath } = globalUser;

  return async (dispatch) => {
    try {
      if (oldPath) {
        const oldPathArray = oldPath.split('/')
        const oldImageRef = firebase.storage().ref(oldPathArray[0]).child(oldPathArray[1]);
        await oldImageRef.delete();
        console.log('@@@DELETED ');
      }
      const timestamp = new Date().getTime();
      const imageRef = firebase.storage().ref('profile_images').child(`${id}_${timestamp}`);
      const uploadImage = await imageRef.put(image);
      const url = await imageRef.getDownloadURL();
      let { path } = imageRef;
      await _updateFirestoreRecord(id, { profilePicture: url, path });
      const globalUser = await _readFirestoreUserRecord(id);
      dispatch({
        type: UPDATE_IMAGE,
        globalUser
      })
    } catch (err) {
      console.log('@@@err: ', err);
    }
  }
}

export const updateProfileData = (globalUser, data) => {
  const { id } = globalUser;
  console.log('@@@data', data);
  console.log('@@@id', id);
  return async (dispatch) => {
    try {
      await _updateFirestoreRecord(id, data);
      const globalUser = await _readFirestoreUserRecord(id);
      dispatch({
        type: UPDATE_DATA,
        globalUser
      })
    } catch (err) {
      console.log('@@@err: ', err);
    }
  }
}

export const logIn = (email, password) => {
  return async (dispatch) => {
    try {
      const authUser = await firebase.auth().signInWithEmailAndPassword(email, password);
      console.log('@@@log in success');

      console.log('@@@authUser.user.uid', authUser.user.uid);

      const globalUser = await _readFirestoreUserRecord(authUser.user.uid);

      console.log('@@@globalUser', globalUser);

      dispatch({
        type: LOG_IN,
        globalUser,
        error: null,
        status: !!globalUser
      })

    } catch (err) {
      console.log('err log in', err.message);
      dispatch({
        type: LOG_IN,
        globalUser: null,
        error: err.message,
        status: false
      })
    }
  }
}

export const uptateGlobalUser = () => {
  return async (dispatch) => {
    try {
      await firebase.auth().onAuthStateChanged(async (globalUser) => {
        globalUser = await _readFirestoreUserRecord(globalUser.uid);
        dispatch({
          type: LOG_IN,
          globalUser,
          error: null,
          status: !!globalUser
        })
      });
    } catch (err) {
      dispatch({
        type: LOG_IN,
        globalUser: null,
        error: err.message,
        status: false
      })
    }
  }
}

export const loginStatus = (status, globalUser = null) => {
  try {
    return async (dispatch) => {
      globalUser = await _readFirestoreUserRecord(globalUser.uid);
      dispatch({
        type: IS_LOGGED,
        status,
        globalUser
      })
    }
  } catch (err) {
    console.log('@@@err', err);
  }
}

export const signUp = (email, password) => {
  return async (dispatch) => {
    try {
      const authUser = await firebase.auth().createUserWithEmailAndPassword(email, password);

      console.log('@@@sign up success');

      const globalUser = {
        email: email,
        fullName: null,
        DoB: null,
        profilePicture: null,
        path: null,
        id: authUser.user.uid,
        createdAt: new Date().getTime()
      }

      await _createFirestoreUserRecord(globalUser);

      dispatch({
        type: SIGN_UP,
        globalUser,
        status: true,
        error: null
      })

    } catch (err) {
      console.log('err signing up', err);
      dispatch({
        type: SIGN_UP,
        error: err.message,
        globalUser: null,
        status: false
      })
    }
  }
}

export const logOut = () => {
  return async (dispatch) => {
    await firebase.auth().signOut();
    dispatch({
      type: LOG_OUT,
      globalUser: null,
      status: false
    })
  }
}

export const restorePassword = (email) => {
  return async (dispatch) => {
    try {
      await firebase.auth().sendPasswordResetEmail(email);
      dispatch({
        type: RESTORE_PASSWORD,
        error: null
      })
    } catch (err) {
      console.log('@@@restore pass error', err.message);
      dispatch({
        type: RESTORE_PASSWORD,
        error: err.message
      })
    }
  }
}