import axios from "axios";

const GET_RATES = 'GET_RATES';
const CHANGE_LOADING_CURRENCIES_DATA_STATUS = 'CHANGE_LOADING_CURRENCIES_DATA_STATUS';
const CHANGE_TARGET_CURRENCY = 'CHANGE_TARGET_CURRENCY';

// change loading status for displaying or not displaying loading spinner
export const changeLodaingDataStatusCurrencies = () => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_LOADING_CURRENCIES_DATA_STATUS,
    })
  }
}
// get reaces shedule from api
export const getRates = (baseCurrency = 'USD') => {
  return async (dispatch) => {
    try {
      const ratesResponse = await axios({
        method: 'get',
        url: `https://api.exchangerate-api.com/v4/latest/${baseCurrency}`,
      });
      const { rates } = ratesResponse.data;
      dispatch({
        type: GET_RATES,
        baseCurrency,
        rates,
      })
    } catch (err) {
      console.log('err', err);
    }
  }
}

export const changeTargetCurrency = (targetCurrency = 'USD') => {
  return async (dispatch) => {
    dispatch({
      type: CHANGE_TARGET_CURRENCY,
      targetCurrency
    })
  }
}