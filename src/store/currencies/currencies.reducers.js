const initialState = {
  baseCurrency: 'USD',
  loadingData: false,
  targetCurrency: '',
  rates: {}
};

export const currencies = (state = initialState, action) => {
  console.log('@@@ACTION', action.type);
  console.log('@@@state', state);
  switch (action.type) {

    case 'GET_RATES':
      return {
        ...state,
        rates: action.rates,
        baseCurrency: action.baseCurrency,
        loadingData: false,
      }

    case 'CHANGE_TARGET_CURRENCY': {
      return {
        ...state,
        targetCurrency: action.targetCurrency,
        loadingData: false
      }
    }

    case 'CHANGE_LOADING_SHEDULES_DATA_STATUS': {
      return {
        ...state,
        loadingData: !state.loadingData,
      }
    }

    default:
      return state;
  };
};