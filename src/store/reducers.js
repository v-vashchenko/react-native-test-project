import { combineReducers } from 'redux';
import { currencies } from './currencies/currencies.reducers';
import { user } from './user/user.reducers';
import { books } from './books/books.reducers';

const rootReducer = combineReducers({
  currencies,
  user,
  books,
});


export default rootReducer;
