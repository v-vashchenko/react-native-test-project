import axios from "axios";
import moment from 'moment'
import firebase from 'react-native-firebase';
import { _createFirestoreUserRecord, _readFirestoreUserRecord, _updateFirestoreRecord } from '../../api/FirestoreActions';

const GET_CATEGORIES = 'GET_CATEGORIES';
const CHANGE_LOADING_BOOKS_DATA_STATUS = 'CHANGE_LOADING_BOOKS_DATA_STATUS';
const GET_BOOKS = 'GET_BOOKS';
const SELECT_BOOK = 'SELECT_BOOK';
const STAR_BOOK = 'STAR_BOOK';

// change loading status for displaying or not displaying loading spinner
export const changeBooksLodaingDataStatus = () => {
  return (dispatch) => {
    dispatch({
      type: CHANGE_LOADING_BOOKS_DATA_STATUS,
    })
  }
}
// get reaces shedule from api
export const getCategories = (baseCategory = '') => {
  return async (dispatch) => {
    try {
      const ratesResponse = await firebase.functions().httpsCallable('categoriesList')({});
      const { data: categoriesList } = ratesResponse;
      console.log('@@@categoriesList', categoriesList);
      dispatch({
        type: GET_CATEGORIES,
        categoriesList,
      })
    } catch (err) {
      console.log('err', err);
    }
  }
}

export const getBooks = (listName = '', offset = 0) => {
  return async (dispatch) => {
    try {

      const getBooks = firebase.functions().httpsCallable('booksList');

      const { data: { results } } = await getBooks({ listName, offset });

      const {
        books: booksList,
        list_name_encoded: currentCategoryEncoded,
        display_name: currentCategory,
        normal_list_ends_at: count
      } = results;

      dispatch({
        type: GET_BOOKS,
        booksList,
        currentCategory,
        currentCategoryEncoded,
        count
      })

    } catch (err) {
      console.log('err', err);
    }
  }
}

export const selectBook = (bookData = {}) => {
  return async (dispatch) => {
    dispatch({
      type: SELECT_BOOK,
      bookData
    });
  }
}

export const starBook = (globalUser = null, bookData = {}) => {
  return async (dispatch) => {
    if (!globalUser) {
      return
    }
    const id = globalUser.id ? globalUser.id : globalUser.uid;
    const { starredBooks = [] } = globalUser;
    const isStarred = starredBooks.findIndex(el => el.primary_isbn10 == bookData.primary_isbn10);
    if (~isStarred) {
      starredBooks.splice(isStarred, 1);
    } else {
      starredBooks.push(bookData);
    }
    await _updateFirestoreRecord(id, { starredBooks });
    dispatch({
      type: STAR_BOOK,
    });
  }
}

export const changeCurrentCategory = (targetCategory = '') => {
  return async (dispatch) => {
    dispatch({
      type: CHANGE_TARGET_CURRENCY,
      targetCurrency
    })
  }
}