const initialState = {
  currentCategory: '',
  currentCategoryEncoded: '',
  loadingData: false,
  categoriesList: [],
  booksList: [],
  rates: {},
  bookData: {},
  count: 0
};

export const books = (state = initialState, action) => {
  console.log('@@@BOOKS REDUCER', action.type);
  console.log('@@@BOOKS REDUCER STATE', state);
  switch (action.type) {

    case 'GET_CATEGORIES':
      return {
        ...state,
        categoriesList: action.categoriesList,
        loadingData: false,
      }

    case 'GET_BOOKS':
      return {
        ...state,
        booksList: action.booksList,
        currentCategory: action.currentCategory,
        currentCategoryEncoded: action.currentCategoryEncoded,
        count: action.count,
        loadingData: false,
      }

    case 'SELECT_BOOK': {
      return {
        ...state,
        loadingData: false,
        bookData: action.bookData
      }
    }

    case 'STAR_BOOK': {
      return {
        ...state,
        loadingData: false,
      }
    }

    case 'CHANGE_LOADING_BOOKS_DATA_STATUS': {
      return {
        ...state,
        loadingData: true,
      }
    }

    default:
      return state;
  };
};